#!/bin/python
## BSD License
## All rights reserved.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##     * Redistributions of source code must retain the above copyright
##       notice, this list of conditions and the following disclaimer.
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in the
##       documentation and/or other materials provided with the distribution.
##     * Neither the name of the the organization nor the
##       names of its contributors may be used to endorse or promote products
##       derived from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY Frank Lizth Garcia Fernandez ``AS IS'' AND ANY
## EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
## WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
## DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
## DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
## (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
## LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
## ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

###############################################################
####   Frank Lizth Garcia Fernandez * flginventor@gmail.com   #
###############################################################
#
#   Little class to stupidly fuzz files
#



import sys,os
import random

class mutator():
    def mutate(self, data):
	return data


class file_mutator():
    def __init__(self, mutator, original_filename, fuzzed_filename=None, bytes_cap=None):
        #FIXME Should take the whole path in consideration
	self.original_dir = os.path.dirname(original_filename)
	self.original_file = os.path.splitext(os.path.basename(original_filename))[0]
        self.original_ext = os.path.splitext(original_filename)[1]
	self.new_filename=self.original_file+"_fuzz"
        self.new_file_dir = "./"
	self.new_file_ext = self.original_ext
	
        if fuzzed_filename:
	    self.new_file_name = os.path.splitext(os.path.basename(fuzzed_filename))[0]
	    self.new_file_ext = os.path.splitext(os.path.basename(fuzzed_filename))[1]
	    self.new_file_dir = os.path.dirname(fuzzed_filename)
	    	    
	self.mutator=mutator
	if bytes_cap:
    	    self.binary_data = open(original_filename, "rb").read(bytes_cap)
	else:
	    self.binary_data = open(original_filename, "rb").read()
	
    def generate(self, seed):
	file = open(self.new_file_dir +os.path.sep + self.new_filename + "%08x"%seed + self.original_ext , "wb")
	file.write("".join(self.mutator.mutate(list(self.binary_data),seed))) # Writes the fuzzed file to disk
        file.close()
#	return os.path.basename(file.name)
	return os.path.basename(file.name)


class random_bit_mutator:
    """
    """

    def __init__(self, bits_to_fuzz=1,seed=0):
        self.bits_to_fuzz = bits_to_fuzz  #percentage of the data
	self.seed=seed

    def _setbit(self,byte,wich):
        return byte | 1<< wich

    def _unsetbit(self,byte,wich):
        return byte & (0xff ^ (1<< wich))


    def _swap_bit(self, byte, vpos):
	byte=ord(byte)
        bit = byte >> vpos & 1
        if bit == 1:
            byte = self._unsetbit(byte, vpos)
        else:
            byte = self._setbit(byte, vpos)
        return chr(byte)

    def mutate(self, binary_data, seed):
	random.seed(seed)
        for n in range(0,1+self.bits_to_fuzz * len(binary_data)/100 ):
            rand_pos = random.randrange(0, len(binary_data)*8) #uniform distro
            binary_data[rand_pos/8] = self._swap_bit(binary_data[rand_pos/8],rand_pos%8)    
	return binary_data

class bytes_mutator:
    """
    """

    def __init__(self, bytes="%n"):
        self.bytes=bytes

    def mutate(self, binary_data, seed):
        binary_data[seed%len(binary_data):seed%len(binary_data)+len(self.bytes)] = self.bytes
	return binary_data

class no_mutator:
    """
    """
    def mutate(self, binary_data, seed):
	return binary_data


class mutator_stack:
    """
    """
    def __init__(self, mutators):
	self.mutators=mutators
    def mutate(self,data,seed):
	for i in range(0,len(self.mutators)):
	    data = self.mutators[i].mutate(data,seed)
	return data


class mutator_list:
    def __init__(self, mutators):
	self.mutators=mutators
    def mutate(self,data,seed):
	return self.mutators[seed%len(self.mutators)].mutate(data,seed/len(self.mutators))

class mutator_serialize:
    def __init__(self, mutator1, porc ,mutator2):
	self.mutator1=mutator1
	self.mutator2=mutator2
	self.porc=porc
    def mutate(self,data,seed):
	data[0:self.porc*len(data)/100] = self.mutator1.mutate(data[0:self.porc*len(data)/100],seed)
	data[self.porc*len(data)/100:] = self.mutator1.mutate(data[self.porc*len(data)/100:],seed)
	return data

def mutator_cap(mutator,N):
    return  mutator_serialize(mutator,N,no_mutator)

bytes_mut = bytes_mutator()
bytes_mutperc = bytes_mutator(bytes="%s")
rnd_bits = random_bit_mutator(bits_to_fuzz=0.1)
mutators = mutator_list([bytes_mutperc,rnd_bits])
#mutators = rnd_bits
#file_mut = file_mutator(mutators,sys.argv[1])
#file_mut = file_mutator(mutator_cap(mutators,20),sys.argv[1])


#for i in range(0,100):
#    print file_mut.generate(i)


def f(arg,dirname,fnames):
    for name in fnames:
	if os.path.isfile(dirname+os.path.sep+name):
	    arg.append(dirname+os.path.sep+name)
	    del name
files=[]
os.path.walk(sys.argv[1],f,files)
print files


from ptrace.debugger import *
from signal import *
from threading import *
from logging import (getLogger, StreamHandler, DEBUG, INFO, WARNING, ERROR)
from sys import stdout

logger = getLogger()
handler = StreamHandler(stdout)
logger.addHandler(handler)

#level = INFO
level = ERROR
#level = WARNING
#level = DEBUG
logger.setLevel(level)
														

debugger = PtraceDebugger()
 
def timeout():
    try:
	for p in debugger.list:
#    	    p.kill(SIGTERM)
    	    p.kill(SIGKILL)
    except Exception,e :
	print e
	pass
#    debugger.terminate(debugger.last.pid)
#    debugger.quit(terminate=True)
## TODO
# pass potentially  2 pipes to interact with th app send instant esc or something
# prepare a simple config file
# rank tthe crash - analize it a bit
# save the result in a db and erase the crashing file
# write code to regenerate a crashing file from the db or seed+origfile
apps = [
	 {
	 "name":  "tar",
	 "cmd" : "/usr/bin/tar tzf  %s",
	 "timeout": 3
	 },

	 ]


#apps = [apps[3]]
runsize=20
co=10000000
random.seed(13511653)
random.shuffle(files)

for ap in apps:
    try:
	os.mkdir(ap['name'])
    except:
	pass

while True:
    for orig in files:
	file_mut = file_mutator(mutators,orig,bytes_cap=1000000)
	for i in range(co*runsize, runsize*(co+1)):
	    keep = False
	    name = file_mut.generate(i)
	    for ap in apps:
		cmd = (ap["cmd"]%name).split() 
		debugger.addProcess(Application(cmd,no_stdout=True))
	        t = Timer(ap["timeout"], timeout)
		print "COMMENCING!!! file %s, app: %s"%(name,ap['name'])
	        t.start() # after 30 seconds, "hello, world" will be printed
	        #event = debugger.waitSignals(SIGTRAP)
	        #debugger.list[0].cont()
    		for k in debugger.dict.keys():
		    try:
			debugger.dict[k].cont()
		    except Exception,e:
			print e
			pass
	        while len(debugger.list) > 0:
		    try:
	
	     		event = debugger.waitSignals(SIGBUS,SIGSEGV,SIGILL,SIGABRT)
			t.cancel()
			os.system("cp %s %s"%(name,ap['name']))
			print "WTF!??  CRASHHHHHHHHHHHHHH","%s %s"%(name,ap['name'])
			print event
			try:
		    	    event.display()
			except:
		    	    passvent.process.dumpRegs()
			#print event.process.dumpMaps()
	    		t.cancel()
			debugger.quit(terminate=True)
			
	    	    except process_event.ProcessExit, e:
			print "Exit",e
		    except process_event.NewProcessEvent, e:
			print "CONT?"
			e.process.cont()
			print "CONTED!"
	    	    except Exception,e:
		        print "Timeout",e
		    print debugger.list
    		    for k in debugger.dict.keys():
			try:
			    print debugger.dict[k],"is_running",debugger.dict[k].running
			    print debugger.dict[k],"is_attached",debugger.dict[k].is_attached
			    debugger.dict[k].cont()
			except Exception,e:
			    print e
			    pass				
	        t.cancel()
	        t.cancel()
	        debugger.quit(terminate=True)
		import os
		try:
		    while True:
			pid,status = os.waitpid(-1,0)
			os.kill(pid,9)
		except Exception, e:
		    print e
		    pass
		print "=============QUIT!======================="

#	    if not keep:
	    os.unlink(name)
	co=co+1